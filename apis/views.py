from django.shortcuts import render,redirect
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import *
from .serializers import *
# Create your views here.

class EmpView(APIView):
    def get(self,request):
        mydata=Employee.objects.all()
        serializer=EmployeeSerializer(mydata,many=True)
        return Response({'status':200,'payload':serializer.data,'message':'success'})
    
    def post(self,request):
        serializer=EmployeeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
        return Response({'status':200,'payload':serializer.data})
    
    def delete(self,request):
        id=request.GET.get('id')
        student_ob=Employee.objects.get(id=id)
        student_ob.delete()
        return Response({'message':'deleted'})
    
